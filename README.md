# ⚙ HTML, CSS and SASS Boilerplate 
This project is used as a boilerplate for tasks in the "HTML, CSS and SASS" course in boom.dev

🤯💥💣

## HTML & CSS Task
**Using Google Fonts(link)**
## Objective
* Checkout the dev branch.
* Add the "Roboto" font from google fonts by using a link meta tag
* Set the HTML document's font to Roboto, sans-serif
* When implemented merge the dev branch to master.
## Requirements
* Start the project with **npm run start**.
* The website must contain link meta tag which adds the Roboto font from google's servers.
